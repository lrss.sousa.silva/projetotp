const express = require('express');
const port = 8000;
let app = express();

app.get('/', (req, res) => {
    res.send("Resultado GET RAIZ");
    console.log("GET /");
});

app.get('/clientes', (req, res) => {
    let obj = req.query;
    console.log(obj);
    res.send("Consultando clientes");
    console.log("GET /clientes");
});

app.listen(port, () => {
    console.log(`Esta é a porta ${port}`)
})